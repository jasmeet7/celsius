package sheridan;

public class Celsius {

	public static void main(String[] args) {
		
		
		System.out.println(convertFromFahrenheit(7));
		
	}
	
	public static int convertFromFahrenheit(int  fahrenheitTemp) {
		
		if (fahrenheitTemp < 0) {
			throw new NumberFormatException("Negative Calues Cannot Be Converted");
		}
		else {
			int celsiusTemp = 0;
			
			double convertedTemp = (Math.round(((double)fahrenheitTemp - 32) * 5/9)*100)/100;
		
			String getDecimalValue = String.valueOf(convertedTemp);
			
			getDecimalValue = getDecimalValue.substring ( getDecimalValue.indexOf ( "." ) + 1 );
	        
	        int decimalValue = Integer.parseInt(getDecimalValue);
	        
	        if (decimalValue > 5) {
	        	
	        	celsiusTemp = (int) Math.ceil(((double)fahrenheitTemp - 32) * 5/9);
	        			
			}else {
				
				celsiusTemp = (int) Math.floor(((double)fahrenheitTemp - 32) * 5/9);
				
			}
			
			return celsiusTemp;
		}
	}

}
