package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testConvertFromFahrenheit() {
		int celsiusTemp = Celsius.convertFromFahrenheit(32);
		System.out.println(celsiusTemp);
		assertTrue("Converted Temprature is good", celsiusTemp == 0);
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryIn() {
		int celsiusTemp = Celsius.convertFromFahrenheit(7);
		System.out.println(celsiusTemp);
		assertTrue("Converted Temprature is good", celsiusTemp == -14);
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryOut() {
		int celsiusTemp = Celsius.convertFromFahrenheit(6);
		System.out.println(celsiusTemp);
		assertFalse("Converted Temprature is good", celsiusTemp == -14);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testConvertFromFahrenheitException() {
		int celsiusTemp = Celsius.convertFromFahrenheit(-6);
	}

}
